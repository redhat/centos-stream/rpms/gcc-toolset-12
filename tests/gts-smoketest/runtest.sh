#!/bin/sh

set -e
set -x

# This is a metapackage.  It's main purpose is to pull other packages
# into play via the rpm deps.
rpm -q gcc-toolset-12
echo
echo RESULT: PASS
